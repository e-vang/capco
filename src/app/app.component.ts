import { Component, OnInit } from '@angular/core';
import { SubscriptionHandler } from './shared/model/subscription-handler.model';
import { UserService } from './services/user.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  subscriptionHandler: SubscriptionHandler = new SubscriptionHandler();
  title = 'User List';
  columns: string[];
  datasource: string[];
  numRows: number[];

  constructor( private userService: UserService) {}

  ngOnInit() {
  this.columns = this.userService.getColumns();
  this.numRows = this.userService.getNumRows();
  this.getUsers();
  }

getUsers() {
  this.subscriptionHandler.addSubscription(
    this.userService.getUser().subscribe( data => {
      this.datasource = data;
    })
  );
}

}
