import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVICE_URL } from '../shared/config/serviceURL';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient ) { }

  getUser(){
    return this.http.get<any>( SERVICE_URL.forGetUsersJSON);
  }

  getColumns(): string[]{
    return [ 'id',"name", "phone", "email", "company",'city', 'status'];
  }

  getNumRows(): number[]{
    return [ 5, 10, 15, 20, 50, 70, 100, 200];
  }

}
