import { Subscription } from 'rxjs';

export class SubscriptionHandler {

    private subscriptions: Array<Subscription>;

    constructor() {
        this.subscriptions = [];
    }

    addSubscription( sub: Subscription): SubscriptionHandler {
        if ( sub ) {
            this.subscriptions.push(sub);
        }
        return this;
    }

    unsubscribeAll() {
        this.subscriptions.forEach( sub => {
            if( sub && sub.unsubscribe ) {
                sub.unsubscribe();
            }
        });
        this.subscriptions = null;
    }
}
